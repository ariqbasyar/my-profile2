from django.urls import path
from . import views

urlpatterns = [
    path(r'', views.index, name='home'),
    path(r'signup/', views.signup, name='signup'),
    path(r'projects/', views.myProject, name='projects'),
    path(r'projectForm/', views.projectForm, name='projectForm')
]
handler404 = 'myprofile.views.handler404'
