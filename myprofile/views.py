from __future__ import unicode_literals
from django.shortcuts import render, redirect
from . import views
from django.template import RequestContext
from .forms import ProjectForm
from datetime import datetime
from .models import Projects
now = datetime.now()
context = {'now': now}


def handler404(request, exception, template_name="404.html"):
    response = render_to_response("404.html")
    response.status_code = 404
    return response

def index(request):
    return render(request, 'index.html')

def not_found(request):
    return render(request, '404.html')

def signup(request):
    return render(request, 'signup.html')

def myProject(request):
    projects_context = {
        'projects_active' : 'active',
        'context' : context,
        'projects': Projects.objects.all().values(),
        }
    return render(request, 'projects.html', projects_context)

def projectForm(request):
    if request.method == "POST" :
        value = {}
        form = ProjectForm(request.POST)
        if form.is_valid():
            value['project_name'] = form.cleaned_data['project_name']
            value['tanggal_mulai'] = form.cleaned_data['tanggal_mulai']
            value['tanggal_selesai'] = form.cleaned_data['tanggal_selesai']
            value['description'] = form.cleaned_data['description']
            value['tempat'] = form.cleaned_data['tempat']
            value['category'] = form.cleaned_data['category']

            project = Projects(
                project_name=value['project_name'],
                tanggal_mulai=value['tanggal_mulai'],
                tanggal_selesai=value['tanggal_selesai'],
                description=value['description'],
                tempat=value['tempat'],
                category=value['category'],
            )
            project.save()
            return redirect('../projects')
    else:
        form = ProjectForm()
    projects_context = {
        'projects_active' : 'active',
        'context' : context,
        'projects': form,
        }
    return render(request, 'form.html', projects_context)
