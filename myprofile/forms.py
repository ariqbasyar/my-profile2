from django import forms
from .models import Projects


YEARS= [x for x in range(2000,2040)]

class ProjectForm(forms.Form):
    attrs={'class' : 'project-input-text'}
    project_name = forms.CharField(
        label='Nama Projek',
        max_length=100,
        required=True,
        widget=forms.TextInput(attrs=attrs)
        )
    tanggal_mulai = forms.DateField(
        label='Tanggal Mulai',
        initial="2019-03-07",
        widget=forms.SelectDateWidget(years=YEARS, attrs=attrs)
        )
    tanggal_selesai = forms.DateField(
        label='Tanggal Selesai',
        initial="2019-03-07",
        widget=forms.SelectDateWidget(years=YEARS, attrs=attrs)
        )
    description = forms.CharField(
        label='Deskripsi Projek',
        required=True,
        widget=forms.Textarea(attrs=attrs),
        )
    tempat = forms.CharField(
        label='Tempat',
        max_length=100,
        required=True,
        widget=forms.TextInput(attrs=attrs)
        )
    category = forms.ChoiceField(
        label='Kategori',
        choices=[('pribadi', 'pribadi'),
                ('kelompok', 'kelompok')]
        )

    class Meta:
        model = Projects
        fields = ('project_name', 'tanggal_mulai', 'tanggal_selesai', 'description', 'tempat', 'category')
