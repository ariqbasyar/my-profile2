from django.db import models


class Projects(models.Model):
    project_name = models.CharField(max_length=100)
    tanggal_mulai = models.DateField(("Date"), auto_now_add=False)
    tanggal_selesai = models.DateField(("Date"), auto_now_add=False)
    description = models.TextField()
    tempat = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
