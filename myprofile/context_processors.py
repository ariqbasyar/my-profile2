from datetime import datetime

def datetimes(request):
    myDate = datetime.now()

    return {
        'date': myDate
    }
